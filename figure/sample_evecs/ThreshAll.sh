#!/bin/bash
ThresholdImage 3 ../../data/seg.nii.gz cortex.nii.gz 2 2 
for TYPE in pca ica 
do
  ThresholdImage 3 cbf_${TYPE}comp01.nii.gz cbf_${TYPE}comp01_posmask.nii.gz 0.1 Inf
  ImageMath 3 cbf_${TYPE}comp01_pos.nii.gz m cbf_${TYPE}comp01_posmask.nii.gz cbf_${TYPE}comp01.nii.gz
  ImageMath 3 cbf_${TYPE}comp01_pos.nii.gz GD cbf_${TYPE}comp01_pos.nii.gz 2
  ImageMath 3 cbf_${TYPE}comp01_pos.nii.gz m cbf_${TYPE}comp01_pos.nii.gz cortex.nii.gz
  ThresholdImage 3 cbf_${TYPE}comp01.nii.gz cbf_${TYPE}comp01_negmask.nii.gz -10000 -0.1
  ImageMath 3 cbf_${TYPE}comp01_neg.nii.gz m cbf_${TYPE}comp01_negmask.nii.gz cbf_${TYPE}comp01.nii.gz
  ImageMath 3 cbf_${TYPE}comp01_neg.nii.gz abs cbf_${TYPE}comp01_neg.nii.gz
  ImageMath 3 cbf_${TYPE}comp01_neg.nii.gz GD cbf_${TYPE}comp01_neg.nii.gz 2
  ImageMath 3 cbf_${TYPE}comp01_neg.nii.gz m cbf_${TYPE}comp01_neg.nii.gz cortex.nii.gz
done

ImageMath 3 thick_fused_eigimg01_dilate.nii.gz GD thick_fused_eigimg01.nii.gz 2
ImageMath 3 thick_fused_eigimg01_dilate.nii.gz m thick_fused_eigimg01_dilate.nii.gz cortex.nii.gz

